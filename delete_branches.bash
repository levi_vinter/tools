#!/bin/bash

# Get a list of all local git branches
branches=$(git branch | sed 's/\*//g') 

# Iterate through each branch
for branch in $branches; do
    if [ "$branch" == "dev" ] || [ "$branch" == "sit" ] || [ "$branch" == "uat" ]; then
        continue
    fi

    # Prompt the user
    read -p "Do you want to delete the branch $branch? (y/n): " answer
    
    # Check if the answer is 'y' or 'Y'
    if [[ $answer =~ ^[Yy]$ ]]; then
        # Delete the branch
        git branch -d "$branch"
        echo "Branch $branch deleted."
    else
        echo "Branch $branch not deleted."
    fi
done
