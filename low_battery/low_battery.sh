#!/bin/bash

if ! command -v acpi 1>/dev/null 2>&1; then
  exit 1
fi

ACPI_OUTPUT=$(acpi -b)
if [[ $(echo "$ACPI_OUTPUT" | grep "Charging") ]]; then
  exit 0
fi

BATTERY_PERCENTAGE=$(echo "$ACPI_OUTPUT" | cut -d "," -f2 | sed 's/%//g' | tr -d " ")

if [[ "$BATTERY_PERCENTAGE" -lt 10 ]]; then
  notify-send -t 5000 -u critical "Low battery" "This would probably be a good time to start charging your computer"
fi

echo "Battery level is at $BATTERY_PERCENTAGE%"
