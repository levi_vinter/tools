#!/usr/bin/env bash

# Make your script exit when a command fails.
# Add || true to commands that you allow to fail.
set -e
# Exit when using undeclared variables.
set -u
# Catch pipe fails in e.g. mysqldump | gzip.
set -o pipefail
# Trace each command in script.
# set -x

MOONLANDER_INPUT=$(xinput | grep 'ZSA Moonlander Mark I' || true)

if [ "$MOONLANDER_INPUT" ]; then
    xmodmap $HOME/.Xmodmap
else
    xmodmap $HOME/.Xmodmap-Colemak
fi

exec xset r rate 200 30
