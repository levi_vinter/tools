#!/usr/bin/env python

import sys
from xml.dom.minidom import parseString

def pretty_print_xml_from_stdin():
    # Read XML data from stdin
    xml_data = sys.stdin.read()

    try:
        # Parse the XML data and pretty print it
        dom = parseString(xml_data)
        pretty_xml_as_string = dom.toprettyxml()
        print(pretty_xml_as_string)
    except Exception as e:
        print("An error occurred while parsing the XML:", str(e), file=sys.stderr)

if __name__ == "__main__":
    pretty_print_xml_from_stdin()

