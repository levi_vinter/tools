#!/bin/bash

# Tool to go to a directory faster
# Depended on fzf (https://github.com/junegunn/fzf)

MAX_DEPTH=2
SEARCH_DIR=~/Dev
RE_INTEGER='^[0-9]+$'

if [[ $# -gt 0 && $1 =~ $RE_INTEGER ]]; then
    MAX_DEPTH=$1
fi

if [[ $# -gt 1 ]]; then
    SEARCH_DIR=$2
fi

DESTINATION=$(find "$SEARCH_DIR" -maxdepth "$MAX_DEPTH" -type d -not -iwholename "*.git*" -printf '%P\n' | fzf -i)

if [[ -z "$DESTINATION" ]]; then
    exit 1
fi

TARGET_DIR="$SEARCH_DIR/$DESTINATION"

if [[ ! -d "$TARGET_DIR" ]]; then
    exit 1
fi

echo "$TARGET_DIR"

cd "$TARGET_DIR"
