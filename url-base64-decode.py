#!/usr/bin/env python

import urllib.parse
import base64
import sys

def decode_string(encoded_str):
    # URL-decode the string
    url_decoded = urllib.parse.unquote(encoded_str)
    
    # Base64-decode the URL-decoded string
    base64_decoded = base64.b64decode(url_decoded).decode('utf-8')
    
    return base64_decoded

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python decode.py <urlencoded_string>")
        sys.exit(1)
    
    encoded_str = sys.argv[1]
    try:
        decoded_str = decode_string(encoded_str)
        print(decoded_str)
    except Exception as e:
        print("Error decoding string:", str(e))

