#!/bin/bash

# Path to the file containing the datetime
FILE_PATH="$HOME/.aws/credentials"

# Extract the datetime string from the file
datetime_str=$(grep 'x_security_token_expires =' "$FILE_PATH" | sed 's/x_security_token_expires = //')

# Convert the datetime string to a Unix timestamp
datetime_ts=$(date -d "$datetime_str" +%s)

# Get the current Unix timestamp
current_ts=$(date +%s)

# Calculate the difference in seconds
diff=$(( datetime_ts - current_ts ))

# Check if there are less than 30 minutes until the datetime
if [ "$diff" -le 1800 ]; then
    echo "AWS session ends/ended at ${datetime_str}"
    exit 1
fi
